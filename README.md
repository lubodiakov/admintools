### What is this repository for? ###

* Tools to simplify repetitive server administration tasks. Shell scripts unless otherwise noted. For Ubuntu or OS X unless otherwise noted.
* Version 1

### How do I get set up? ###

* Dependencies
* Any Operating system (e.g. Ubuntu, OS X, etc.) with a POSIX shell (e.g. bash) and built in commands used in scripts, such as uname, generally speaking.
* See descriptions of each shell script for more specific information.

* Download a shell script into a disk with read/write access. Shell scripts generate output, such as text files, and need to write it to disk.
* Make sure script has execute priviledges, i.e. chmod u+x. Otherwise it can't run.
* Read the script if you want to know what it does, either for security or out of curiosity.

### Contribution guidelines ###

* Feedback if encountering bugs welcome. Scripts tested on my own computers, or dog-fooded personally as they say. :-)
* Suggestions to enhance/add features welcome. Code to do so, even more!

### Who do I talk to? ###

* Lubo Diakov lubodiakov at gmail dot com

### List-installed-packages.sh ###

* This shell script displays in terminal and records to a text file:
* uname -a, lsb_release -cd, df -lh sorted by size of partition, and manually installed packages, alphabetized
* Designed on Ubuntu, but should work on any Debian based distribution. It may give blanks for some fields like lsb_release
* but the list of .debs should work on any Debian derivative, and things like uname, df should work even on non-Debian systems.
* Also uses scp to transfer to ssh server, address is hardcoded in script for now. See TODO at the end of the script.
* Reports are saved in your Downloads folder and named yourhostname-date(in yyyy-mm-dd format)-manually-installed-packages.txt.
* If run more than once per 24 hours, new runs are appended to existing file with timestamp like: 2018-07-04T04:10:42+03:00
* The script is free to copy/use/modify under the terms of the GPLv2 license. Copyright, Lubo Diakov.

* # TODO
* Make more OS agnostic with an text menu to select OS at the beginning, and by add similar commands to list packages for Fedora .rpm, OS X .pkg, etc.
* Make menu to ask if output should be sent via scp, defaults to Yes. And Menu for ssh server address, with default choice as well.