#!/usr/bin/env bash
# This shell script cobbled together from bits and bobs by Lubo Diakov aka lubod @ ubuntuforums.org
# It displays in terminal and records to a text file:
# uname -a, lsb_release -cd, df -lh sorted by size of partition, and manually installed deb. packages, alphabetized
# Designed on Ubuntu, but should work on any Debian based distribution. It may give blanks for some fields like lsb_release
# but the list of .debs should work on any Debian derivative, and things like uname, df should work even on non-Debian systems.
# Also uses scp to transfer to ssh server, address is hardcoded in script for now. See TODO at the end of this file.
# Reports are saved in your Downloads folder and named yourhostname-date(in yyyy-mm-dd format)-manually-installed-packages.txt.
# If run more than once per 24 hours, new runs are appended to existing file with timestamp like: 2018-07-04T04:10:42+03:00
# The script is free to copy/use/modify under the terms of the GPLv2 license. Copyright, Lubo Diakov.
# Contact author: lubodiakov at gmail dot com
# Revision 1, 2018-07-04

echo "Report"-$(date -Iseconds) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo $(uname -a) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo $(lsb_release -cd) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
df -lh|sort -uhrk2,1 |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
df -lh|sort -uhrk2,2|grep /dev/ |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "Manually installed packages, alphabetized:" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) | tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt
scp -C ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt lubod@xenial1:/home/lubod/Downloads

# One-liner, for interactive shell use. (replace txt\n with txt; to crunch into one-liner!) see: https://superuser.com/questions/962323/replace-with-a-newline

#echo "Report"-$(date -Iseconds) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo $(uname -a) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo $(lsb_release -cd) |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;df -lh|sort -uhrk2,1 |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;df -lh|sort -uhrk2,2|grep /dev/ |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "Manually installed packages, alphabetized:" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;echo "" |tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) | tee -a ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt;scp -C ~/Downloads/$(hostname)-$(date --rfc-3339 date)-manually-installed-packages.txt lubod@xenial1:/home/lubod/Downloads

# TODO
# Make more OS agnostic with an interactive menu to select OS at the beginning, and by adding similar commands to package listing routines for Fedora .rpm, OS X .pkg, etc.
# Make menu to ask if output should be sent via scp, defaults to Yes. And Menu for ssh server address, with default choice as well.
